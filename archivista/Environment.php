<?php namespace Archivista;

use App;

class Environment
{
  public static $version = "1.0";
  
  public static function setValue($envKey, $envValue)
  {
    $envFile = app()->environmentFilePath();
    $envcontent = file_get_contents($envFile);

    $str = preg_split('/\s+/', $envcontent);

    foreach($str as $env_key => $env_value)
    {
      // Turn the value into an array and stop after the first split
      // So it's not possible to split e.g. the App-Key by accident
      $entry = explode("=", $env_value, 2);

      // Check, if new key fits the actual .env-key
      if($entry[0] == $envKey)
      {
        // If yes, overwrite it with the new one
        $str[$env_key] = $envKey."=\"".$envValue."\"";
      }
      else
      {
        // If not, keep the old one
        $str[$env_key] = $env_value;
      }
    }

    $envcontent = implode("\n", $str);
    file_put_contents(base_path() . '/.env', $envcontent);
  }

  public static function getValue($envKey)
  {
    return env($envKey);
  }

  public static $reqList = array(
    'php' => '7.3.0',
    'mcrypt' => false,
    'openssl' => true,
    'pdo' => true,
    'mbstring' => true,
    'tokenizer' => true,
    'xml' => true,
    'ctype' => true,
    'json' => true,
    'bcmath' => true,
    'obs' => ''
  );

  public static function _requirements()
  {
    $requirements = array();
    $requirements['php_version'] = version_compare(PHP_VERSION, self::$reqList['php'], ">=");
    $requirements['openssl_enabled'] = extension_loaded("openssl");
    $requirements['pdo_enabled'] = defined('PDO::ATTR_DRIVER_NAME');
    $requirements['mbstring_enabled'] = extension_loaded("mbstring");
    $requirements['tokenizer_enabled'] = extension_loaded("tokenizer");
    $requirements['xml_enabled'] = extension_loaded("xml");
    $requirements['ctype_enabled'] = extension_loaded("ctype");
    $requirements['bcmath_enabled'] = extension_loaded("bcmath");
    $requirements['json_enabled'] = extension_loaded("json");

    if(!self::getValue("APP_DEBUG"))
    {
      $requirements['mod_rewrite_enabled'] = null;
      if (function_exists('apache_get_modules')) {
        $requirements['mod_rewrite_enabled'] = in_array('mod_rewrite', apache_get_modules());
      }
    }

    return $requirements;
  }

  public static function requirements()
  {
    $requirements = self::_requirements();
    foreach($requirements as $k => $v)
    {
      if(!$v)
        return false;
    }

    return true;
  }
}
