<?php namespace Archivista;

use App;

class ApiCall
{
    public static $methods = ["google", "todostuslibros"];

    public $resp = array();
    public function __construct($isbn, $method = "google")
    {
      $this->isbn = $isbn;
      $this->method = $method;

      switch($this->method)
      {
        case "google":
          $this->url = "https://www.googleapis.com/books/v1/volumes?q=isbn:".$this->isbn."&maxResults=1";
          break;
        case "todostuslibros":
          $this->url = "https://www.todostuslibros.com/busquedas?keyword=".$this->isbn;
          break;
      }

      $this->{$this->method}($this->curl($this->url));
    }

    private function todostuslibrosauthors($data)
    {
      $ret = "";

      if(!empty($data))
      {
        $authors = explode("/", $data);
        foreach($authors as $author)
        {
          $words = explode(", ", $author);
          if($ret != "")
          {
            $ret = $ret.", ";
          }

          if(count($words) > 1)
            $ret = $ret.$words[1]." ".$words[0];
          else
            $ret = $ret.$author;
        }
      }

      return $ret;
    }

    private function todostuslibros($raw)
    {
      $raw2 = $this->curl($this->getCadena($raw, 'https://www.todostuslibros.com/libros/', '">'));

      if(!$raw2)
      {
        $this->resp['error'] = "No hay resultados";
        return;
      }

      $this->resp['title'] = $this->getCadenaLim($raw2, '<h1 class="title">', '</h1>');
      $this->resp['descript'] = trim(strip_tags($this->getCadenaLim($raw2, '<div class="col-md-9 synopsis" id="module">', '</div>')));
      if($this->resp['descript'][0] == ">"){$this->resp['descript'] = "";}

      $this->resp['date'] = $this->getCadenaLim($this->getCadenaLim($raw2, '<dt class="col-5 col-md-3 col-lg-5">Fecha publicación :</dt>', 'd>'), 'col-lg-7">', '</d');
      $this->resp['author'] = $this->todostuslibrosauthors($this->getCadenaLim($raw2, 'class="author">', '</a>'));
      $this->resp['isbn'] = $this->getCadenaLim($this->getCadenaLim($raw2, '<dt class="col-5 col-md-3 col-lg-5 d-none d-sm-block">EAN:</dt>', 'd>'), 'd-sm-block">', '</d');
      $this->resp['pagecount'] = $this->getCadenaLim($this->getCadenaLim($raw2, '<dt class="col-5 col-md-3 col-lg-5">Nº páginas:</dt>', 'd>'), 'col-lg-7">', '</d');
    }

    private function google($raw)
    {
      $data = json_decode($raw, true);
      if($data['totalItems'] == 0)
      {
        $this->resp['error'] = "No hay resultados";
      }

      if(isset($data["items"][0]["volumeInfo"]["title"]))
        $this->resp['title'] = $data["items"][0]["volumeInfo"]["title"];

      if(isset($data["items"][0]["volumeInfo"]["publishedDate"]))
        $this->resp['date'] = $data["items"][0]["volumeInfo"]["publishedDate"];

      if(isset($data["items"][0]["volumeInfo"]["industryIdentifiers"][1]["identifier"]))
        $this->resp['isbn'] = $data["items"][0]["volumeInfo"]["industryIdentifiers"][1]["identifier"];

      if(isset($data["items"][0]["volumeInfo"]["pageCount"]))
        $this->resp['pagecount'] = $data["items"][0]["volumeInfo"]["pageCount"];

      if(isset($data["items"][0]["searchInfo"]["textSnippet"]))
        $this->resp['descript'] = $data["items"][0]["searchInfo"]["textSnippet"];

      if(isset($data["items"][0]["volumeInfo"]["authors"]))
      {
        $this->resp['author'] = "";
        foreach($data["items"][0]["volumeInfo"]["authors"] as $autor)
        {
          if($this->resp['author'] != "")
          {
            $this->resp['author'] = $this->resp['author'].", ";
          }

          $this->resp['author'] = $autor;
        }
      }
    }

    private function limpiarString($String)
    {
         $String = str_replace(array("|","|","[","^","´","`","¨","~","]","'","#","{","}","https://",""),"",$String);
         return $String;
    }

    private function curl($url)
    {
      $ch = curl_init();

      // set url
      curl_setopt($ch, CURLOPT_URL, $url);

      //return the transfer as a string
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

      // $output contains the output string
      $output = curl_exec($ch);

      // close curl resource to free up system resources
      curl_close($ch);
      return $output; // Devuelve la información de la función
    }

    private function getCadenaLim($data, $start, $end)
    {
      return substr($this->getCadena($data, $start, $end), strlen($start));
    }

    private function getCadena($data, $start, $end)
    {
      $spos = strpos($data, $start);
      $subraw = substr($data, $spos);
      $epos = strpos($subraw, $end);
      return substr($subraw, 0, $epos);
    }
}
