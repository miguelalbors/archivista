<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listas', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->boolean('visibleReadDates');
            $table->timestamps();
        });

        Schema::create('listabooks', function (Blueprint $table) {
          $table->id();
          $table->integer('listaid');
          $table->integer('bookid');
          $table->string('date_start')->nullable();
          $table->string('date_end')->nullable();
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listas');
        Schema::dropIfExists('listabooks');
    }
}
