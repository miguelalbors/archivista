<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class PrestamosController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function estado()
  {
    return view('prestamos.estado');
  }

  public function chocoleguis()
  {
    return view('prestamos.chocoleguis');
  }

  public function putchocoleguis(Request $request)
  {
    $request->validate([
      'chocolegui_id' => 'required'
    ]);

    $chocolegui = App\Models\Chocolegui::getIfExist($request->chocolegui_id);

    if($request->name != "")
    {
      $chocolegui->name = $request->name;
      $chocolegui->save();
    }
    else
    {
      $chocolegui->delete();
    }

    return redirect()->route("prestamos.chocoleguis");
  }

  public function gestionar()
  {
    return view('prestamos.gestionar');
  }

  public function putgestionar(Request $request)
  {
    $request->validate([
      'book_id' => 'required',
      'colegui_id' => 'required'
    ]);

    $prestamo = new App\Models\Prestamo();
    $prestamo->libro = $request->book_id;
    $prestamo->chocolegui = $request->colegui_id;
    $prestamo->save();

    return redirect()->route("prestamos.gestionar");
  }
}
