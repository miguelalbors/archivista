<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use Archivista;

class GuestApi extends Controller
{
  public function apicall_isbn($isbn, $method = "google")
  {
    $apicall = new Archivista\ApiCall($isbn, $method);
    $resp = $apicall->resp;
    return view('apicall', compact("resp"));
  }

  public function apicall_book($isbn = -1)
  {
    $resp = array();
    if($isbn == -1)
    {
      foreach(App\Models\Book::getAll() as $book)
      {
        $resp[$book->id] = array();
        $resp[$book->id]['title'] = $book->title;
        $resp[$book->id]['date'] = $book->date;
        $resp[$book->id]['pagecount'] = $book->pagecount;
        $resp[$book->id]['editorial'] = $book->editorial;
        $resp[$book->id]['autores'] = $book->getAuthors();
        $resp[$book->id]['genres'] = $book->getGenres();
        $resp[$book->id]['types'] = $book->getTypes();
        $resp[$book->id]['isbn'] = $book->isbn;
      }
    }
    else
    {
      $book = App\Models\Book::where("isbn", "=", $isbn)->first();
      if(empty($book))
      {
        $resp['error'] = "No hay coincidencias";
      }
      else
      {
        $resp[$book->id] = array();
        $resp[$book->id]['title'] = $book->title;
        $resp[$book->id]['date'] = $book->date;
        $resp[$book->id]['pagecount'] = $book->pagecount;
        $resp[$book->id]['editorial'] = $book->editorial;
        $resp[$book->id]['autores'] = $book->getAuthors();
        $resp[$book->id]['genres'] = $book->getGenres();
        $resp[$book->id]['types'] = $book->getTypes();
        $resp[$book->id]['isbn'] = $book->isbn;
      }
    }

    return view('apicall', compact("resp"));
  }
}
