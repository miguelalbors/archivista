<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class AcpApi extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function putUpdateBook($bookid, $field, $value)
  {
    $book = App\Models\Book::find($bookid);
    if(!empty($book) && isset($book->{$field}))
    {
      if($field != "date")
      {
        $book->{$field} = $value;
      }
      else
      {
        $book->setDate($value);
      }
      $book->save();
    }

    $resp = ["msg" => "ok"];
    return view('apicall', compact("resp"));
  }

  public function addAuth($bookid, $authid)
  {
    $book = App\Models\Book::find($bookid);
    $book->addAuthor($authid);

    $resp = ["msg" => "ok"];
    return view('apicall', compact("resp"));
  }
  public function delAuth($bookid, $authid)
  {
    $book = App\Models\Book::find($bookid);
    $book->delAuthor($authid);

    $resp = ["msg" => "ok"];
    return view('apicall', compact("resp"));
  }

  public function addGen($bookid, $genreid)
  {
    $book = App\Models\Book::find($bookid);
    $book->addGenre($genreid);

    $resp = ["msg" => "ok"];
    return view('apicall', compact("resp"));
  }
  public function delGen($bookid, $genreid)
  {
    $book = App\Models\Book::find($bookid);
    $book->delGenre($genreid);

    $resp = ["msg" => "ok"];
    return view('apicall', compact("resp"));
  }

  public function addType($bookid, $typeid)
  {
    $book = App\Models\Book::find($bookid);
    $book->addType($typeid);

    $resp = ["msg" => "ok"];
    return view('apicall', compact("resp"));
  }
  public function delType($bookid, $typeid)
  {
    $book = App\Models\Book::find($bookid);
    $book->delType($typeid);

    $resp = ["msg" => "ok"];
    return view('apicall', compact("resp"));
  }

  public function delPrest($id)
  {
    $prestamo = App\Models\Prestamo::find($id);
    if(!empty($prestamo))
    {
      $prestamo->delete();
    }

    $resp = ["msg" => "ok"];
    return view('apicall', compact("resp"));
  }
}
