<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App;
use Archivista;
use Artisan;

class SetupController extends Controller
{
  protected function checkAuth()
  {
    if(!Auth::check())
    {
      abort(404);
    }
  }

  public function requirements()
  {
    return view('requirements');
  }

  public function setup()
  {
    return view('setup');
  }
  public function migrate()
  {
    Artisan::call('migrate');
    return redirect()->route("register");
  }

  public function save_setup(Request $request)
  {
      $request->validate([
        'dbhost' => 'required',
        'dbname' => 'required',
        'dbusername' => 'required',
        'dbpassword' => 'required',
        'sitename' => 'required',
        'siteurl' => 'required'
      ]);

      Archivista\Environment::setValue("APP_TITLE", $request->sitename);
      Archivista\Environment::setValue("APP_URL", $request->siteurl);
      Archivista\Environment::setValue("DB_HOST", $request->dbhost);
      Archivista\Environment::setValue("DB_DATABASE", $request->dbname);
      Archivista\Environment::setValue("DB_USERNAME", $request->dbusername);
      Archivista\Environment::setValue("DB_PASSWORD", $request->dbpassword);

      Archivista\Environment::setValue("APP_DEBUG", "false");

      return redirect()->route("migrate");
  }

  public function endsetup()
  {
    Archivista\Environment::setValue("APP_CONFIGURED", "true");
    return redirect()->route("index");
  }
}
