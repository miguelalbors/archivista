<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use Archivista;

class GuestController extends Controller
{
  /**
  * Show the application dashboard.
  *
  * @return \Illuminate\Contracts\Support\Renderable
  */
  public function index()
  {
    $books = App\Models\Book::getAll();
    return view('home', compact('books'));
  }

  public function home()
  {
    return redirect()->route("index");
  }

  public function indexFiltered($filter, $value)
  {
    //$val = preg_split("/[\s,]+/", $value);
    $val = explode("+", $value);
    switch($filter)
    {
      case "title":
        $books = App\Models\Book::getFromTitle($val);
        break;
      case "author":
        $books = App\Models\Book::getFromAuthor($val);
        break;
      case "authgenre":
        $books = App\Models\Book::getFromAuthorGenre($val);
        break;
      case "editorial":
        $books = App\Models\Book::getFromEditorial($val);
        break;
      case "genre":
        $books = App\Models\Book::getFromGenre($val);
        break;
      case "type":
        $books = App\Models\Book::getFromType($val);
        break;
      default:
        $books = App\Models\Book::getAll();
    }

    return view('home', compact('books'));
  }
}
