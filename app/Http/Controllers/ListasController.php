<?php

namespace App\Http\Controllers;

use Monarobase\CountryList\CountryListFacade;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App;

class ListasController extends Controller
{
  protected function checkAuth()
  {
    if(!Auth::check())
    {
      abort(404);
    }
  }

  public function listas()
  {
    return view('listas.listas');
  }

  public function view($id)
  {
    $lista = App\Models\Lista::find($id);

    if(!empty($lista))
    {
      $paises = CountryListFacade::getList("es");
      return view('listas.view', compact("lista", "paises"));
    }

    return redirect()->route("listas.list");
  }

  public function putlistas(Request $request)
  {
    $this->checkAuth();

    $request->validate([
      'lista_id' => 'required'
    ]);

    if($request->lista_id != -1)
    {
      if(empty($request->name))
      {
        $lista = App\Models\Lista::find($request->lista_id);
        if(!empty($lista))
        {
          $lista->elimina();
        }
      }
    }
    else if(!empty($request->name))
    {
      $lista = new App\Models\Lista;
      $lista->name = $request->name;
      $lista->visibleReadDates = isset($request->lista_view);
      $lista->save();
    }

    return redirect()->route("listas.list");
  }

  public function putbookinlista(Request $request)
  {
    $this->checkAuth();

    $request->validate([
      'lista_id' => 'required',
      'lista_action' => 'required'
    ]);

    $lista = App\Models\Lista::find($request->lista_id);
    if(!empty($lista))
    {
      if($request->lista_action == "add")
      {
        $request->validate([
          'book_id' => 'required',
          'start' => 'required',
          'end' => 'required'
        ]);

        $lista->addBook($request->book_id, $request->start, $request->end);
      }
      else
      {
        $request->validate([
          'rm_book' => 'required',
        ]);

        $lista->rmBook($request->rm_book);
      }
    }

    return redirect()->route("listas.view", $request->lista_id);
  }
}
