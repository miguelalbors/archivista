<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Autor extends Model
{
    use HasFactory;

    private static $genres = [-1 => "Sin asignar", 0 => "Señor", 1 => "Señora", 2 => "Señore"];
    private static $genresymbol = [-1 => "", 0 => "&#9794;", 1 => "&#9792;", 2 => "nb"];

    public function getGenre()
    {
        return self::$genres[$this->genre];
    }

    public static function getGenreName($genreid)
    {
        return self::$genres[$genreid];
    }

    public static function getGenreId($genrename)
    {
      foreach(self::$genres as $k => $g)
      {
        if($genrename == $g)
          return $k;
      }

      return -2;
    }

    public static function getGenreSymbol($genreid)
    {
        return self::$genresymbol[$genreid];
    }

    public function elimina()
    {
      foreach(Book::getFromAuthor([$this->name]) as $b)
      {
        $b->delAuthor($this->id);
      }

      $this->delete();
    }

    public static function getAll()
    {
      return Autor::orderBy('name')->get();
    }

    public static function getIdOf(string $name)
    {
      $tmp = Autor::where('name', '=', $name)->first();
      if(empty($tmp))
      {
        $auth = new Autor;
        $auth->name = $name;
        $auth->genre = -1;
        $auth->save();
        return $auth->id;
      }
      return $tmp->id;
    }

    public static function getIfExist(string $id)
    {
      $tmp = Autor::find($id);
      if(empty($tmp))
      {
        return new Autor;
      }
      return $tmp;
    }
}
