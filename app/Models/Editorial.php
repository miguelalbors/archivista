<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Editorial extends Model
{
    use HasFactory;

    public function elimina()
    {
      foreach(Book::where('editorial', '=', $this->id)->get() as $b)
      {
        $b->editorial = -1;
        $b->save();
      }

      $this->delete();
    }

    public static function getAll()
    {
      return Editorial::orderBy('name')->get();
    }

    public static function getName($id)
    {
      $ed = Editorial::find($id);
      return (!empty($ed)) ? $ed->name : "";

    }

    public static function getId($name)
    {
      return Editorial::where('name', '=', $name)->first()->id;
    }

    public static function getIfExist(string $id)
    {
      $tmp = Editorial::find($id);
      if(empty($tmp))
      {
        return new Editorial;
      }
      return $tmp;
    }
}
