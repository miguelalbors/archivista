<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Prestamo extends Model
{
    use HasFactory;

    public static function getAll()
    {
      return Prestamo::join("books", "books.id", "=", "prestamos.libro")->select("prestamos.id as id", "prestamos.libro as libro", "prestamos.chocolegui as chocolegui")->orderBy('books.title')->get();
    }

    public function getBook()
    {
      return Book::find($this->libro);
    }

    public function getChocolegui()
    {
      return Chocolegui::find($this->chocolegui);
    }

    public static function getFromChocolegui($chocolegui)
    {
      return Prestamo::where("chocolegui", "=", $chocolegui)->get();
    }
}
