<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TipoObra extends Model
{
    use HasFactory;

    public static function getAll()
    {
      return TipoObra::join("genres", "tipo_obras.genre_id", "=", "genres.id")->select("tipo_obras.id as id", "tipo_obras.genre_id as genre_id", "genres.name as name")->orderBy("genres.name")->get();
    }

    public static function getCandidates()
    {
      $lib = TipoObra::select("genre_id")->get()->toArray();
      return Genre::whereNotIn("id", $lib)->orderBy("name")->get();
    }

    public static function getIfExist(string $id)
    {
      $tmp = TipoObra::find($id);
      if(empty($tmp))
      {
        return new TipoObra;
      }
      return $tmp;
    }
}
