<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tipo extends Model
{
    use HasFactory;

    public function elimina()
    {
      foreach(Book::getFromType([$this->name]) as $b)
      {
        $b->delType($this->id);
      }

      $this->delete();
    }

    public static function getAll()
    {
      return Tipo::orderBy('name')->get();
    }

    public static function getId($name)
    {
      return Tipo::where('name', '=', $name)->first()->id;
    }

    public static function getIfExist(string $id)
    {
      $tmp = Tipo::find($id);
      if(empty($tmp))
      {
        return new Tipo;
      }
      return $tmp;
    }
}
