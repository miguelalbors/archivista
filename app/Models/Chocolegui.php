<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Chocolegui extends Model
{
    use HasFactory;

    public static function getAll()
    {
      return Chocolegui::orderBy('name')->get();
    }

    public static function getIfExist(string $id)
    {
      $tmp = Chocolegui::find($id);
      if(empty($tmp))
      {
        return new Chocolegui;
      }
      return $tmp;
    }

    public static function conPrestamo()
    {
      return Chocolegui::join("prestamos", "prestamos.chocolegui", "=", "chocoleguis.id")->select("chocoleguis.id as id", "chocoleguis.name as name")->distinct()->get();
    }
}
