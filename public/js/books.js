function actualiza(route, id_libro, id_data)
{
  $.get(route+"/"+id_libro+"/"+id_data, function(output)
  {
    location.reload();
  });
}
function delauth(id_libro, id_autor)
{
  actualiza("/api/acp/delAuth", id_libro, id_autor);
}
function addauth(id_libro, id_autor)
{
  actualiza("/api/acp/addAuth", id_libro, id_autor);
}

function delgen(id_libro, id_gen)
{
  actualiza("/api/acp/delGen", id_libro, id_gen);
}
function addgen(id_libro, id_gen)
{
  actualiza("/api/acp/addGen", id_libro, id_gen);
}

function deltype(id_libro, id_type)
{
  actualiza("/api/acp/delType", id_libro, id_type);
}
function addtype(id_libro, id_type)
{
  actualiza("/api/acp/addType", id_libro, id_type);
}

function updateField(id_libro, id_field)
{
  actualiza("/api/acp/updateField", id_libro, id_field+"/"+$("#"+id_field+"_"+id_libro).val());
}

function updateEditorial(id_libro, id_editorial)
{
  actualiza("/api/acp/updateField", id_libro, "editorial/"+id_editorial);
}

function eliminaPrestamo(aid)
{
  $.get("/api/acp/delPrest/"+aid, function(output)
  {
    location.reload();
  });
}

function eliminaLista(lid)
{
  $("#lista_id").val(lid);
  $("#lista_name").val("");
  $("#editor").submit();
}

function eliminaLibroDeLista(bid)
{
  $("#rm_book").val(bid);
  $("#lista_action").val("rm");
  $("#editor").submit();
}
