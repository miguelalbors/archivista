@extends('layouts.app')

@section('content')
<script src="{{ asset('js/books.js') }}" defer></script>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-9">
        <div class="card">
          <div class="card-header bg-primary text-white">
            Gestionar préstamos
          </div>
          <div class="card-body">
            <table class="table table-hover">
              <tbody>
                @php $prestamos=0 @endphp
                @foreach(App\Models\Prestamo::getAll() as $p)
                  @php $prestamos++ @endphp
                  <tr>
                    <td class="align-middle">{{ $p->getBook()->title }}</td>
                    <td class="align-middle">{{ $p->getChocolegui()->name }}</td>
                    <td style="width:25px;" class="align-middle"><button OnClick="eliminaPrestamo({{ $p->id }});" class="btn btn-sm btn-danger" title="Eliminar"><span class="fas fa-times"></span></button></td>
                  </tr>
                  @endforeach
                  @if($prestamos == 0)
                  <tr><td>No hay préstamos activos</td></tr>
                  @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="card">
          <div class="card-header bg-primary text-white">Nuevo préstamo</div>
          <div class="card-body">
            <form id="editor" method="POST" action="{{ route('prestamos.putgestionar') }}">
              @csrf
              @method('PUT')
              <div class="form-group">
                <select id="book_id" name="book_id" class="form-control">
                  <option disabled selected value> Elegir un libro </option>
                  @foreach(App\Models\Book::getNotPrestados() as $b)
                    <option value="{{$b->id}}">{{ $b->title }}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                <select id="colegui_id" name="colegui_id" class="form-control">
                  <option disabled selected value> Elegir un amigo </option>
                  @foreach(App\Models\Chocolegui::getAll() as $c)
                    <option value="{{$c->id}}">{{ $c->name }}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                <input type="submit" class="btn btn-outline-primary form-control" value="Guardar"/>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endsection
