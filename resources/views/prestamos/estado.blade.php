@extends('layouts.app')

@section('content')
<script src="{{ asset('js/books.js') }}" defer></script>
  <div class="container-fluid">
      <div class="card">
        <div class="card-header bg-primary text-white">
          Préstamos activos
        </div>
        <div class="card-body row">
          @php $chocoleguis=0 @endphp
          @foreach(App\Models\Chocolegui::conPrestamo() as $c)
            @php $chocoleguis++ @endphp
            <div class="col-md-3">
              <div class="card">
                <div class="card-header bg-primary text-white">
                  {{ $c->name }}
                </div>
                <div class="card-body">
                  <table class="table table-hover">
                    <tbody>
                      @foreach(App\Models\Prestamo::getFromChocolegui($c->id) as $p)
                      <tr>
                        <td>{{ $p->getBook()->title }}</td>
                        <td style="width:25px;" class="align-middle"><button OnClick="eliminaPrestamo({{ $p->id }});" class="btn btn-sm btn-danger" title="Eliminar"><span class="fas fa-times"></span></button></td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          @endforeach
          @if($chocoleguis == 0)
            <div class="col-md-12">
              No hay préstamos activos
            </div>
          @endif
        </div>
      </div>
    </div>
@endsection
