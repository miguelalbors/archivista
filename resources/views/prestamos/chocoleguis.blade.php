@extends('layouts.app')

@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-9">
        <div class="card">
          <div class="card-header bg-primary text-white">
            Amigos
          </div>
          <div class="card-body">
            <table class="table table-hover">
              <tbody>
                @php $chocoleguis=0 @endphp
                @foreach(App\Models\Chocolegui::getAll() as $c)
                  @php $chocoleguis++ @endphp
                  <tr>
                    <td id="a_{{$c->id}}" OnClick="muestra('{{$c->id}}');" class="align-middle">{{ $c->name }}</td>
                    <td id="b_{{$c->id}}" style="width:25px;" class="align-middle"></td>
                  </tr>
                @endforeach
                @if($chocoleguis == 0)
                <tr><td>No hay amigos</td></tr>
                @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="card">
          <div class="card-header bg-primary text-white">Nuevo amigo</div>
          <div class="card-body">
            <form id="editor" method="POST" action="{{ route('prestamos.putchocoleguis') }}">
              @csrf
              @method('PUT')
              <input id="chocolegui_id" name="chocolegui_id" hidden value="-1"/>
              <div class="form-group">
                <input id="chocolegui_name" name="name" class="form-control" placeholder="Nombre y apellidos"/>
              </div>
              <div class="form-group">
                <input type="submit" class="btn btn-outline-primary form-control" value="Guardar"/>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div hidden>
    <input id="edit_name" name="edit_name" class="form-control" type="text"/>
    <button id="edit_boton" class="btn btn-primary"><span class="far fa-save"></span></button>
  </div>

  <script>
    function muestra(aid)
    {
      value = $("#a_"+aid).html();
      $("#a_"+aid).prop("onclick", null).off("click");

      editor = $("#edit_name").clone();
      editor.val(value.trim());
      $("#a_"+aid).html(editor);

      editor = $("#edit_boton").clone();
      editor.on("click", function(){ guarda(aid); });
      $("#b_"+aid).html(editor);
    }

    function guarda(aid)
    {
      name = $("#a_"+aid).find("#edit_name").val();

      $("#chocolegui_id").val(aid);
      $("#chocolegui_name").val(name);
      $("#editor").submit();
    }
  </script>
  @endsection
