@extends('layouts.app')

@section('content')
  @auth
    <script src="{{ asset('js/books.js') }}" defer></script>
  @endauth
  <div class="container-fluid">
    <div class="row">
      @auth
        <div class="col-md-9">
      @else
        <div class="col-md-12">
      @endauth
        <div class="card">
          <div class="card-header bg-primary text-white">
            Listas
          </div>
          <div class="card-body">
            <table class="table table-hover">
              <tbody>
                @php $listas=0 @endphp
                @foreach(App\Models\Lista::getAll() as $l)
                  @php $listas++ @endphp
                  <tr>
                    <td class="align-middle" OnClick="location.href = '{{route('listas.view', $l->id)}}';">{{ $l->name }}</td>
                    <td style="width:100px;" class="align-middle">
                      <a class="btn btn-sm btn-primary" title="Ver" href="{{route('listas.view', $l->id)}}"><span class="fas fa-eye"></span></a>
                    @auth
                      <button OnClick="eliminaLista({{ $l->id }});" class="btn btn-sm btn-danger" title="Eliminar"><span class="fas fa-times"></span></button>
                    @endauth
                    </td>
                  </tr>
                  @endforeach
                  @if($listas == 0)
                  <tr><td>No hay listas</td></tr>
                  @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>
      @auth
        <div class="col-md-3">
          <div class="card">
            <div class="card-header bg-primary text-white">Nueva lista</div>
            <div class="card-body">
              <form id="editor" method="POST" action="{{ route('listas.putlistas') }}">
                @csrf
                @method('PUT')
                <input id="lista_id" name="lista_id" hidden value="-1"/>
                <div class="form-group">
                  <div class="form-check form-check-inline">
                    <input id="lista_name" name="name" class="form-control" placeholder="Nombre de lista"/>
                  </div>
                  <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="viewCheck" name="lista_view" value="visible" checked/>
                    <label class="form-check-label" for="viewCheck">Fechas</label>
                  </div>
                </div>
                <div class="form-group">
                  <input type="submit" class="btn btn-outline-primary form-control" value="Guardar"/>
                </div>
              </form>
            </div>
          </div>
        </div>
      @endauth
    </div>
  </div>
  @endsection
