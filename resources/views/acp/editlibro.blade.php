@extends('layouts.app')

@section('content')
  @if(!empty($book))
    <div class="container">
      <form id="save-form" method="POST" action="{{route('acp.editbook')}}" enctype="multipart/form-data">
        @method('PUT')
        @csrf
        <input hidden id="id" name="id" value="{{$book->id}}"/>
        <div class="row">
          <div class="col-md-7 text-left">
            <h2>Editar libro</h2>
          </div>
          <div class="col-md-5 text-right">
            <button type="button" OnClick="elimina();" class="btn btn-danger text-white"><span class="far fa-trash-alt"></span></button>
            <button type="submit" class="btn btn-primary">Guardar</button>
          </div>
        </div>
        <hr/>
        <div class="row justify-content-center">
          <div class="col-md-6">
            {!! App\Models\UIAssistant::getInputField("Título", "title", $book->title) !!}
            {!! App\Models\UIAssistant::getMultipleSelectField("Autores", "author", App\Models\UIAssistant::formatArray(App\Models\Autor::getAll(), ['id', 'name']), "150px", $book->getAllAuthorId()) !!}
            {!! App\Models\UIAssistant::getSelectField("Editorial", "editorial", App\Models\UIAssistant::formatArray(App\Models\Editorial::getAll(), ['id', 'name']), "100%", $book->editorial) !!}
            {!! App\Models\UIAssistant::getInputField("ISBN", "isbn", $book->isbn) !!}
            {!! App\Models\UIAssistant::getInputField("Fecha de publicación", "date", $book->date) !!}
            {!! App\Models\UIAssistant::getInputField("Páginas", "pagecount", $book->pagecount) !!}
          </div>
          <div class="col-md-3">
            {!! App\Models\UIAssistant::getMultipleSelectField("Formato", "tipo", App\Models\UIAssistant::formatArray(App\Models\Tipo::getAll(), ['id', 'name']), "425px", $book->getAllTypeId()) !!}
          </div>
          <div class="col-md-3">
            <div class="row">
              {!! App\Models\UIAssistant::getMultipleSelectField("Géneros", "genero", App\Models\UIAssistant::formatArray(App\Models\Genre::getAll(), ['id', 'name']), "425px", $book->getAllGenreId()) !!}
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            {!! App\Models\UIAssistant::getTextField2md("Descripción", "descript", $book->descript, "100%", "200px", "") !!}
          </div>
        </div>
      </form>
      <hr/>
    </div>
  @endif
  <script>
    function elimina()
    {
      $("#isbn").val("-1");
      $("#save-form").submit();
    }
  </script>
@endsection
