@extends('layouts.app')

@section('content')
  @if(!empty($book))
    <div class="container">
        <p>
          Título: {{ $book->title }}
        </p>
        <p>
          Autores: {{ print_r($book->getAuthors()) }}
        </p>
        <p>
          Descripción: {{ $book->descript }}
        </p>
        <p>
          Editorial: {{ $book->editorial }}
        </p>
        <p>
          ISBN: {{ $book->isbn }}
        </p>
        <p>
          Fecha: {{ $book->date }}
        </p>
        <p>
          Nº pag.: {{ $book->pagecount }}
        </p>
        <p>
          Géneros: {{ print_r($book->getGenres()) }}
        </p>
        <p>
          Tipos: {{ print_r($book->getTypes()) }}
        </p>
    </div>
  @endif
@endsection
